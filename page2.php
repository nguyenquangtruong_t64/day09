<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Trang 2</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php
	 $question = array(
        "6" => array(
          "title" => "Tính: 11 x 11 = ?",
          "result" => "B",
          "name" => "Q6",
          "anwser" => array(
            "A" => "111",
            "B" => "121",
            "C" => "236",
            "D" => "245"
          ),
        ),
        "7" => array(
          "title" => "NaCl là công thức hóa học của ?",
          "result" => "D",
          "name" => "Q7",
          "anwser" => array(
            "A" => "Lưu Huỳnh",
            "B" => "Thạch tín",
            "C" => "Thuốc tẩy",
            "D" => "Muối ăn"
          ),
        ),
        "8" => array(
          "title" => "Nguyễn Thị Ánh Viên là vận động viên của môn thể thao nào ?",
          "result" => "A",
          "name" => "Q8",
          "anwser" => array(
            "A" => "Bơi",
            "B" => "Bắn súng",
            "C" => "Điền kinh",
            "D" => "Cờ Vua"
          ),
        ),
        "9" => array(
          "title" => "Tờ tiền nào của Việt Nam có in hình Vịnh Hạ Long ?",
          "result" => "D",
          "name" => "Q9",
          "anwser" => array(
            "A" => "50.000 Đ",
            "B" => "10.000 Đ",
            "C" => "500.000 Đ",
            "D" => "200.000 Đ"
          ),
        ),
        "10" => array(
          "title" => "Ở điều kiện thường, nước sôi ở nhiệt độ là bao nhiêu độ C ?",
          "result" => "C",
          "name" => "Q10",
          "anwser" => array(
            "A" => "120",
            "B" => "90",
            "C" => "100",
            "D" => "250"
          ),
        ),
      );
	?>

    <fieldset class="background">

        <?php
        $data = array();
        $error_data = false;
        $cookie = 0;
        $cookie_name = "result_page2";
        if (!empty($_POST['btnSubmit'])) {
            $data['Q6'] = isset($_POST['Q6']) ? $_POST['Q6'] : '';
            $data['Q7'] = isset($_POST['Q7']) ? $_POST['Q7'] : '';
            $data['Q8'] = isset($_POST['Q8']) ? $_POST['Q8'] : '';
            $data['Q9'] = isset($_POST['Q9']) ? $_POST['Q9'] : '';
            $data['Q10'] = isset($_POST['Q10']) ? $_POST['Q10'] : '';
            foreach ($data as $key => $value) {
            if (empty($value)) {
                $error_data = true;
            }
            }
            if ($error_data == true) {
                echo "<div style='color: red;'>Chưa trả lời hết câu hỏi.</div>";
            } 
            else {
                foreach ($question as $key => $value) {
                    $name = "Q" . $key;
                    if ($value['result'] == $data[$name]) {
                        $cookie = $cookie + 1;
                    }
                }
                setcookie($cookie_name, $cookie, time() + (86400 * 30), "/");
                header("Location: ./page3.php");
            }
        }
        ?>

        <form action="page2.php" method="POST" id="form" enctype="multipart/form-data">
            <div class="container">
                <?php
                foreach ($question as $key => $value) {
                    echo '<p class="question">Câu ' . $key . ': ' . $value['title'] . '</p>';
                    foreach ($value['anwser'] as $keyAnwer => $valueAnwer) {
                        echo '<div class="answerOption">
                            <input type="radio" id="' . $key . '' . $keyAnwer . '" name="' . $value['name'] . '" value="' . $keyAnwer . '">
                            <label for="' . $key . '' . $keyAnwer . '">' . $valueAnwer . '</label>
                        </div>';
                    }
                }
                ?>
            </div>
            <div class="btn">
                <input type="submit" value="Nộp bài" class="btnSubmit" name="btnSubmit" />
            </div>
        </form>
    </fieldset>



</body>

</html>