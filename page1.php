<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Trang 1</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php
	 $question = array(
        "1" => array(
          "title" => "Việt Nam có bao nhiêu tỉnh thành ?",
          "result" => "D",
          "name" => "Q1",
          "anwser" => array(
            "A" => "60",
            "B" => "61",
            "C" => "62",
            "D" => "63"
          ),
        ),
        "2" => array(
          "title" => "Cho hình chữ nhật có chiều dài = 5, chiều rộng = 6. Tính diện tích ?",
          "result" => "A",
          "name" => "Q2",
          "anwser" => array(
            "A" => "30",
            "B" => "11",
            "C" => "22",
            "D" => "40"
          ),
        ),
        "3" => array(
          "title" => "Đâu là một loại nhạc cụ ?",
          "result" => "C",
          "name" => "Q3",
          "anwser" => array(
            "A" => "Bút",
            "B" => "Sách",
            "C" => "Sáo",
            "D" => "Dao"
          ),
        ),
        "4" => array(
          "title" => "Quả cam trong tiếng anh là gì ?",
          "result" => "D",
          "name" => "Q4",
          "anwser" => array(
            "A" => "Lemon",
            "B" => "Apple",
            "C" => "Banana",
            "D" => "Orange"
          ),
        ),
        "5" => array(
          "title" => "Ngày quốc khánh của Việt Nam là ngày bao nhiêu ?",
          "result" => "B",
          "name" => "Q5",
          "anwser" => array(
            "A" => "19/5",
            "B" => "2/9",
            "C" => "30/4",
            "D" => "20/10"
          ),
        ),
      );
	?>

    <fieldset class="background">
    <?php
    $data = array();
    $error_data = false;
    $cookie = 0;
    $cookie_name = "result_page1";
    if (!empty($_POST['btnSubmit'])) {
        $data['Q1'] = isset($_POST['Q1']) ? $_POST['Q1'] : '';
        $data['Q2'] = isset($_POST['Q2']) ? $_POST['Q2'] : '';
        $data['Q3'] = isset($_POST['Q3']) ? $_POST['Q3'] : '';
        $data['Q4'] = isset($_POST['Q4']) ? $_POST['Q4'] : '';
        $data['Q5'] = isset($_POST['Q5']) ? $_POST['Q5'] : '';
        foreach ($data as $key => $value) {
          if (empty($value)) {
            $error_data = true;
          }
        }
        if ($error_data == true) {
          echo "<div style='color: red;'>Chưa trả lời hết câu hỏi.</div>";
        } 
        else {
          foreach ($question as $key => $value) {
            $name = "Q" . $key;
            if ($value['result'] == $data[$name]) {
              $cookie = $cookie + 1;
            }
          }
          setcookie($cookie_name, $cookie, time() + (86400 * 30), "/");
          header("Location: ./page2.php");
        }
      }
    ?>

        <form action="page1.php" method="POST" id="form" enctype="multipart/form-data">
            <div class="container">
                <?php
                foreach ($question as $key => $value) {
                    echo '<p class="question">Câu ' . $key . ': ' . $value['title'] . '</p>';
                    foreach ($value['anwser'] as $keyAnwer => $valueAnwer) {
                        echo '<div class="answerOption">
                            <input type="radio" id="' . $key . '' . $keyAnwer . '" name="' . $value['name'] . '" value="' . $keyAnwer . '">
                            <label for="' . $key . '' . $keyAnwer . '">' . $valueAnwer . '</label>
                        </div>';
                    }
                }
                ?>
            </div>
            <div class="btn">
                <input type="submit" value="Next" class="btnSubmit" name="btnSubmit" />
            </div>
        </form>
    </fieldset>



</body>

</html>