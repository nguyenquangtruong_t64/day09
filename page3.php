<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Trang 3</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
  
    <fieldset class="background">

        <?php
        if (isset($_COOKIE['result_page1']) && isset($_COOKIE['result_page2'])) {
            $result = (int)$_COOKIE['result_page1'] + (int)$_COOKIE['result_page2'];
            $res = $result;
            echo "<p>Số câu đúng là:  $result </p>";
            echo "<p>Điểm: $res </p>";
            if ($res < 4) {
              echo "Bạn quá kém, cần ôn tập thêm";
            } else if ($res < 7) {
              echo "Cũng bình thường";
            } else {
              echo "Sắp sửa làm được trợ giảng lớp PHP";
            }
        }
        ?>
    </fieldset>

</body>

</html>